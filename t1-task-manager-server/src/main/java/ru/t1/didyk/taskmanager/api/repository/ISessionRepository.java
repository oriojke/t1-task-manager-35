package ru.t1.didyk.taskmanager.api.repository;

import ru.t1.didyk.taskmanager.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
