package ru.t1.didyk.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.didyk.taskmanager.marker.UnitCategory;
import ru.t1.didyk.taskmanager.repository.ProjectRepository;
import ru.t1.didyk.taskmanager.repository.TaskRepository;

import static ru.t1.didyk.taskmanager.constant.ProjectTestData.*;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final TaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Before
    public void before() {
        taskRepository.add(USER_TASK);
        taskRepository.add(ADMIN_TASK);
        projectRepository.add(USER_PROJECT);
        projectRepository.add(ADMIN_PROJECT);
    }

    @After
    public void after() {
        taskRepository.clear();
        projectRepository.clear();
    }

    @Test
    public void bindTaskToProjectTest() {
        projectTaskService.bindTaskToProject(USER2.getId(), ADMIN_PROJECT.getId(), ADMIN_TASK.getId());
        Assert.assertEquals(ADMIN_PROJECT.getId(), ADMIN_TASK.getProjectId());
    }

    @Test
    public void removeProjectByIdTest() {
        projectTaskService.removeProjectById(USER1.getId(), USER_PROJECT.getId());
        Assert.assertTrue(projectRepository.findOneById(USER_PROJECT.getId()) == null);
    }

    @Test
    public void unbindTaskFromProjectTest() {
        projectTaskService.unbindTaskFromProject(USER1.getId(), USER_PROJECT.getId(), USER_TASK.getId());
        Assert.assertNotEquals(USER_PROJECT.getId(), USER_TASK.getProjectId());
    }

}
