package ru.t1.didyk.taskmanager.constant;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.endpoint.IAuthEndpoint;
import ru.t1.didyk.taskmanager.api.service.IPropertyService;
import ru.t1.didyk.taskmanager.dto.request.UserLoginRequest;
import ru.t1.didyk.taskmanager.service.PropertyService;

public class SharedTestMethods {

    @NotNull
    private final static IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final static IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    public static String loginUser(@NotNull String login, @NotNull String password) {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin("test");
        request.setPassword("test");
        final String userToken = authEndpoint.login(request).getToken();
        return userToken;
    }

}
