package ru.t1.didyk.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.didyk.taskmanager.api.endpoint.ITaskEndpoint;
import ru.t1.didyk.taskmanager.api.service.IPropertyService;
import ru.t1.didyk.taskmanager.constant.SharedTestMethods;
import ru.t1.didyk.taskmanager.dto.request.*;
import ru.t1.didyk.taskmanager.dto.response.*;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.marker.IntegrationCategory;
import ru.t1.didyk.taskmanager.model.Task;
import ru.t1.didyk.taskmanager.service.PropertyService;

import java.util.List;

@Category(IntegrationCategory.class)
public class TaskEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);

    @Before
    public void before() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull TaskCreateRequest request = new TaskCreateRequest(userToken);
        request.setName("DEFAULT NAME");
        request.setDescription("DEFAULT DESC");
        taskEndpoint.createTask(request);
    }

    public String getTaskId() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull TaskListRequest listRequest = new TaskListRequest(userToken);
        listRequest.setSortType(Sort.BY_NAME);
        @Nullable final TaskListResponse listResponse = taskEndpoint.listTasks(listRequest);
        List<Task> tasks = listResponse.getTasks();
        @Nullable final Task task = tasks.get(0);
        return task.getId();
    }

    @Test
    public void bindToProjectTest() {

    }

    @Test
    public void changeTaskStatusByIdTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(userToken);
        request.setId(getTaskId());
        request.setStatus(Status.COMPLETED);
        @Nullable final TaskChangeStatusByIdResponse response = taskEndpoint.changeTaskStatusById(request);
        @Nullable final Task newTask = response.getTask();
        Assert.assertNotNull(newTask);
        Assert.assertEquals(Status.COMPLETED, newTask.getStatus());
    }

    @Test
    public void changeTaskStatusByIndexTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(userToken);
        request.setIndex(0);
        request.setStatus(Status.COMPLETED);
        @Nullable final TaskChangeStatusByIndexResponse response = taskEndpoint.changeTaskStatusByIndex(request);
        @Nullable final Task newTask = response.getTask();
        Assert.assertNotNull(newTask);
        Assert.assertEquals(Status.COMPLETED, newTask.getStatus());
    }

    @Test
    public void clearTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull TaskClearRequest request = new TaskClearRequest(userToken);
        @Nullable TaskClearResponse response = taskEndpoint.clear(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void completeTaskByIdTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(userToken);
        request.setId(getTaskId());
        @Nullable final TaskCompleteByIdResponse response = taskEndpoint.completeTaskById(request);
        @Nullable final Task newTask = response.getTask();
        Assert.assertNotNull(newTask);
        Assert.assertEquals(Status.COMPLETED, newTask.getStatus());
    }

    @Test
    public void completeTaskByIndexTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(userToken);
        request.setIndex(0);
        @Nullable final TaskCompleteByIndexResponse response = taskEndpoint.completeTaskByIndex(request);
        @Nullable final Task newTask = response.getTask();
        Assert.assertNotNull(newTask);
        Assert.assertEquals(Status.COMPLETED, newTask.getStatus());
    }

    @Test
    public void createTaskTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull TaskCreateRequest request = new TaskCreateRequest(userToken);
        request.setName("NEW NAME");
        request.setDescription("NEW DESC");
        @Nullable final TaskCreateResponse response = taskEndpoint.createTask(request);
        @Nullable final Task newTask = response.getTask();
        Assert.assertNotNull(newTask);
    }

    @Test
    public void listTasksTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull TaskListRequest listRequest = new TaskListRequest(userToken);
        listRequest.setSortType(Sort.BY_NAME);
        @Nullable final TaskListResponse listResponse = taskEndpoint.listTasks(listRequest);
        List<Task> tasks = listResponse.getTasks();
        Assert.assertNotNull(tasks);
    }

    @Test
    public void removeTaskByIdTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(userToken);
        request.setId(getTaskId());
        @Nullable final TaskRemoveByIdResponse response = taskEndpoint.removeTaskById(request);
        @Nullable final Task newTask = response.getTask();
        Assert.assertNotNull(newTask);
    }

    @Test
    public void removeTaskByIndexTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(userToken);
        request.setIndex(0);
        @Nullable final TaskRemoveByIndexResponse response = taskEndpoint.removeTaskByIndex(request);
        @Nullable final Task newTask = response.getTask();
        Assert.assertNotNull(newTask);
    }

    @Test
    public void showTaskByIdTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull TaskShowByIdRequest request = new TaskShowByIdRequest(userToken);
        request.setId(getTaskId());
        @Nullable final TaskShowByIdResponse response = taskEndpoint.showTaskById(request);
        @Nullable final Task newTask = response.getTask();
        Assert.assertNotNull(newTask);
    }

    @Test
    public void showTaskByIndexTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull TaskShowByIndexRequest request = new TaskShowByIndexRequest(userToken);
        request.setIndex(0);
        @Nullable final TaskShowByIndexResponse response = taskEndpoint.showTaskByIndex(request);
        @Nullable final Task newTask = response.getTask();
        Assert.assertNotNull(newTask);
    }

    @Test
    public void showByProjectIdTest() {

    }

    @Test
    public void startTaskByIdTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull TaskStartByIdRequest request = new TaskStartByIdRequest(userToken);
        request.setId(getTaskId());
        @Nullable final TaskStartByIdResponse response = taskEndpoint.startTaskById(request);
        @Nullable final Task newTask = response.getTask();
        Assert.assertNotNull(newTask);
        Assert.assertEquals(Status.IN_PROGRESS, newTask.getStatus());
    }

    @Test
    public void startTaskByIndexTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull TaskStartByIndexRequest request = new TaskStartByIndexRequest(userToken);
        request.setIndex(0);
        @Nullable final TaskStartByIndexResponse response = taskEndpoint.startTaskByIndex(request);
        @Nullable final Task newTask = response.getTask();
        Assert.assertNotNull(newTask);
        Assert.assertEquals(Status.IN_PROGRESS, newTask.getStatus());
    }

    @Test
    public void unbindFromProjectTest() {

    }

    @Test
    public void updateTaskByIdTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(userToken);
        request.setId(getTaskId());
        request.setName("NEW TEST NAME");
        request.setDescription("NEW TEST DESCRIPTION");
        @Nullable final TaskUpdateByIdResponse response = taskEndpoint.updateTaskById(request);
        @Nullable final Task newTask = response.getTask();
        Assert.assertNotNull(newTask);
        Assert.assertEquals("NEW TEST NAME", newTask.getName());
        Assert.assertEquals("NEW TEST DESCRIPTION", newTask.getDescription());
    }

    @Test
    public void updateTaskByIndexTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test", "test");
        @NotNull TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(userToken);
        request.setIndex(0);
        request.setName("NEW TEST NAME-2");
        request.setDescription("NEW TEST DESCRIPTION-2");
        @Nullable final TaskUpdateByIndexResponse response = taskEndpoint.updateTaskByIndex(request);
        @Nullable final Task newTask = response.getTask();
        Assert.assertNotNull(newTask);
        Assert.assertEquals("NEW TEST NAME-2", newTask.getName());
        Assert.assertEquals("NEW TEST DESCRIPTION-2", newTask.getDescription());
    }
}
